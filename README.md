# Go Button

![Go Button](supplemental/assembly/completed-go-button--01.jpg)

## Overview

This repository contains instructions and guidelines for building a "Go Button" for Go Baby Go 
cars. All materials are documented within the provided 
<[fabrication](fabrication)> and <[supplemental](supplemental)> folders.


## What is a "Go Button"?

A "Go Button" provides an interface for child with mobility issues to drive a Go Baby Go car. It is 
intended to replace the foot pedal in a Go Baby Go car build.

These cars typically run on a single 6V or 12V battery and use 1-2 brushed or brushless motors. To 
drive the vehicle, the child presses on the foot pedal to engage the motor and steers via the 
steering wheel.

The Go Baby Go project is aimed at customizing these cars for children who are unable to engage the 
foot pedal on their own. The "Go Button" is part of the custom wiring that allows the child to 
drive the car.

The "Go Button" is specifically designed to:
   -   Have a large surface area
   -   Be easy to actuate with a light press
   -   Be brightly colored to stand out from the rest of the car
   -   Easily repairable or swappable

At this time, the "Go Button" does not provide any steering control. However, it does attach rather 
easily to the face of a steering wheel via Velcro. In this position, many children can steer while 
pressing the button. For those who are unable to steer, guide carts and parental help can assist.

The plug on the "Go Button" mates with a "Go Button Jack" if installed in the car (please see the 
external "Wiring Harnesses" repository for details on wiring up the corresponding "Go Button Jack".


## How to Use this Project

Please see the [README](fabrication/README.md) in the fabrication section to get started. When 
providing a "Go Button" to a child, please consider providing a variety of colors to let them select 
the color that they are most drawn towards.


## Version Information

This is version 1.0 of this guide. Please see the [Changelog](Changelog.md) for a detailed version 
history.


## License

This project and materials are released under the [Unlicense](LICENSE). We provide the materials 
included here as a reference to anyone working in the Go Baby Go space. We hope this serves as a 
stepping stone to improved designs and would love to hear back on any improvements made on what we 
have created here.


