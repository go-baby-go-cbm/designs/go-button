# Overview

This directory contains all the materials and instructions you should need to make a new "Go 
Button" from scratch:

-   <[Enclosure](enclosure)>:
    -   Contains the [STL files](enclosure/stl-files) needed to print the current design.
    -   Contains the SolidWorks [CAD design files](enclosure/cad-files) for modifying any of the 
        design to better fit your needs.

-   <[Guides](guides)>:
    -   Contain instructions on how to [wire](guides/wire-harness.pptx), 
        [assemble](guides/assembly.pptx), and [validate](guides/validation.pptx) a new "GoButton".

-   [Materials](materials.xlsx):
    -   Spreadsheet detailing all components and tools needed to fabricate a new "Go Button" from 
        scratch.


