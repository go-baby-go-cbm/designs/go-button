# Changelog

----------------------------------------------------------------------------------------------------
## v1.0 [2023-03-12 20:45:00]

### Added
-   A list of materials used to fabricate a "Go Button".
-   Summary guides for fabricating a "Go Button". Guides include:
    -   How to make the wire harness.
    -   How to assemble the 3D printed parts and the wire harness.
    -   How to validate the "Go Button".
-   Supplemental photos that cover the printing, wiring, assembly, and validation of a new "Go 
    Button".
-   A project README and a READEME file for each of the root folders in this repository.


----------------------------------------------------------------------------------------------------
## v0.12 [2023-02-26 14:30:00]

### Deleted
-   Enclosure:
    -   Button - Main:
        -   Removed both the 55-degree and 60-degree overhang versions to improve clarity (no 
            practical need found for the higher angles and more shallow of an anlge is preferred for 
            the intent of the design).


----------------------------------------------------------------------------------------------------
## v0.11 [2021-10-27 14:00:00]

### Added
-   Enclosure:
    -   Added SolidWorks design files for all components.
    -   Button - Main:
        -   Added 55-degree and 60-degree overhang versions of this component (higher angles should 
            bea easier to print without support).

### Changed
-   Enclosure:
    -   Button - Main:
        -   Removed fillet on top face of component (better results without this fillet when 
            printing without support material).
        -   Renamed what was formerly "button--main--middle-fillet-only.*" to 
            "button--main--53-degree-overhang.*".

### Deleted
-   Enclosure:
    -   Button - Main:
        -   Test STL files used to evaluate the impact of chamfers on printing. Opting to go with 
            the version "--middle-fillet-only".


----------------------------------------------------------------------------------------------------
## v0.10 [2020-11-20 15:45:00]

### Added
-   Enclosure:
    -   Test prints to allow exploring the impact of fillets on 3D prints of the Button - Main 
        component.
    -   Button - Main
        -   Additional enclosure print with the fillet along the middle of the Button - Main body 
            removed (STL file with suffix "--middle-fillet-only").
        -   Additional enclosure print with both fillets removed (STL file with suffix 
            "--no-fillets").

### Changed
-   Enclosure:
    -   Button - Main:
        -   Changed the baseline angle for all three Button - Main STL files to 53 degrees.
        -   Renamed the STL file with both fillets retained to have the suffix "--baseline".


----------------------------------------------------------------------------------------------------
## v0.9 [2020-11-18 10:10:00]

### Changed
-   Enclosure:
    -   Button - Main:
        -   Switched from a fillet to a chamfer on the outer edge of button to allow printing 
            without support material. 
            -   The fillet provides a 50-degree angle for the overhang it creates when printing on 
                the top face of the component (on most printers, an angle of 45-degrees or higher 
                allows for printing without support material).


----------------------------------------------------------------------------------------------------
## v0.8 [2020-11-17 09:45:00]

### Changed
-   Enclosure:
    -   Interior Wire Cover:
        -   Decreased the distance between the two posts used to mate with the Base.
            -   This change was missed when tweaking the Base in the last revision.


----------------------------------------------------------------------------------------------------
## v0.7 [2020-11-14 10:20:00]

### Changed
-   Enclosure:
    -   Base:
        -   Added two additional attachment points (post + pilot hole) for the Button Locking Ring.
        -   Tweaked position of internal features (e.g. wire opening) for better alignment with the 
            two additional attachment points.
        -   Reduced the distance between the two wire strain relief posts.
    -   Button - Lip:
        -   Added two more attachment points to secure the lip to the main body of the button.
        -   Decreased the size of the cutout for clearance around the wire opening in the Base.
        -   Added fillets along the edges for the cutout around the wire opening in the Base.
    -   Button - Locking Ring:
        -   Added two more attachment points to mate with the additional two attachment points on 
            the Base.
        -   Added fillet along outer edge to reduce sharp points.
    -   Button - Main:
        -   Added two more attachment points to secure the main button body to the lip.
    -   Exterior Wire Clip:
        -   Slightly increased size of the component.
        -   Added fillets around point that comes near the wire.

### Notes
-   Testing revealed it was too easy to pull the button part way out of the base. Direction of this 
    revision is to add two more mount points for all components to better secure things/handle the 
    pliability of the printed plastic.


----------------------------------------------------------------------------------------------------
## v0.6 [2020-11-12 15:35:00]

### Changed
-   Enclosure:
    -   Button - Lip STL:
        -   Added cutout to allow the button to actuate without hitting the wires.

### Deleted
-   Enclosure:
    -   Dropped the Cherry MX Key Brace component.
        -   Not needed and not worth the hassle for the little gain it provides.


----------------------------------------------------------------------------------------------------
## v0.5 [2020-11-10 11:20:00]

### Added
-   Enclosure:
    -   Button - Lip STL:
        -   Attached to the bottom of the button to provide a lip which interacts with the Button's 
            locking ring to keep the button installed.
    -   Button - Locking Ring STL:
        -   Attaches to top of base to secure the Button feature.
        -   Interacts with the Button's lip to keep the Button installed.
    -   Button - Main STL:
        -   Body of the button (main actuator pressed by a child using a "Go Button").

### Changed
-   Enclosure:
    -   Base:
        -   Dropped screw holes for the Cherry MX Key Brace mount points.
        -   Added two attachment points (post + pilot hole) for the new Button - Locking Ring.
        -   Moved the position of the Interior Wire Cover inwards for better clearance around the 
            button when actuating.
        -   Removed fillet around top edge as it will now be flush against the new Button - Locking 
            Ring.
    -   Cherry MX Key Brace:
        -   Increased height by 1.4 mm.
    -   Exterior Wire Clip:
        -   Dropped cutout for wire on point of clip (a flatter face better for assembly and strain 
            relief).
        -   Stretched out top face so fills in more of the gap on the Base once installed.
        -   Removed fillets.

### Deleted
-   Enclosure:
    -   The two STL files used to provide spacers for the Cherry MX Key Brace.

### Notes
-   Big pivot to going from gluing in an existing small frisbee to fully designing the button from 
    scratch.


----------------------------------------------------------------------------------------------------
## v0.4 [2020-10-11 14:00:00]

### Added
-   Enclosure:
    -   A STL composed of a set of 0.6 mm spacers for the Cherry MX Key Brace (to test if increasing 
        height by 0.6 mm would improve assembly).
    -   A STL composed of a set of 0.8 mm spacers for the Cherry MX Key Brace (to test if increasing 
        height by 0.8 mm would improve assembly).

### Changed
-   Enclosure:
    -   Dropped the "--baseline" string from the Cherry MX Key Brace STL file.
        -   Going with original brace design while evaluating increasing the height of the brace.

### Deleted
-   Enclosure:
    -   The test STL files for reducing the Cherry MX Key Brace height.


----------------------------------------------------------------------------------------------------
## v0.3 [2020-10-01 15:30:00]

### Added
-   Enclosure:
    -   New STL files that reduce the height of the Cherry MX Key Brace by 0.5 mm and 1.0 mm (test 
        prints).

### Changed
-   Enclosure:
    -   Renamed the existing Cherry MX Key Brace STL to have the suffix "--baseline".


----------------------------------------------------------------------------------------------------
## v0.2 [2020-09-30 08:50:00]

### Changed
-   Enclosure:
    -   Base
        -   Made slots in holder for bottom of Cherry MX Keys (so don't need to trim plastic posts on 
            the switches before installation).
        -   Created guide holes for the Interior Wire Clip.
        -   Added fillets to the area for the wire slot.
    -   Exterior Wire Clip:
        -   Expanded size so less of a gap between this and base when installed.
        -   Fillet added to entire part.
    -   Interior Wire Cover:
        -   Added posts to make it easier to install/attach.
        -   Fillet added to edges which may touch the wire.


----------------------------------------------------------------------------------------------------
## v0.1 [2020-09-28 16:10:00]

### Added
-   A licensing file (opting to use the 'unlicense' license).
-   Enclosure:
    -   New STL files for:
        -   Base
        -   Cherry MX Key Brace
        -   Exterior Wire Clip
        -   Interior Wire Clip


